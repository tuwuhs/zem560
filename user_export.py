#!/usr/bin/env python

from struct import pack, unpack
import csv
import zkem.zkem as zkem

# Set clock IP address here
SERVER = ''

f = open('users.csv', 'wt')
w = csv.writer(f)

w.writerow(['userid', 'role', 'code', 'name', 'pasnr', 'groupid', 'userid'])

term = zkem.zem560()
if(term.connect(SERVER)):
    if(term.get_user_data()):
       usr = term.unpack_user_data()
       for i in usr:
           w.writerow(i)

    term.disconnect()

else:
    print 'Failed to connect to %s' % SERVER

f.close()
